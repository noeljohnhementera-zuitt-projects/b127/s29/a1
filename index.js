
//Start of Activity at Line 96

const express = require("express");
// create an application using express
// allows access sa express.js
// ready may na

const app = express()
// This creates an express aplication and stores this as a constant called app
// app is our server
// sa app variable ilagay ang functions, methods, etc

const port = 3000;
// we need a port in order for our applicaiton to run 

// >>Nethods used from express JS are middlewares<<
	// middleware is a software that provides services outside of what's offered by the operating system

app.use(express.json())
// lahat ng data na dumadaan sa API
// Setup for allowing the server to handle data from requests
// Allows your app to read json data

app.use(express.urlencoded({ extended:true }))
// Allows your data to read daya from forms
// By default, information received from the url can only be received as a string or an array
/*By applying the optiong of "extended true", this allows us to receive information
in other data types such as an object or boolean etc, which we will use
throughout our applicaiton*/


// We will now create routes
// Express has methods corresponding to HTTP methods

app.get("/", (req, res) => {
// This route expects to receive a GET request at the base UR: "/"
// "http://localhost:3000/"
	res.send("Hello World");
})
// .send = uses the express JS modules method instead to send a response back to the client
// .end naman sa node

app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoiont!");
})

// Post Method = always needs to have a body
app.post("/hello", (req, res)=>{
	res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`)
})

// Post for /signup
// Mock database
let users = [];

app.post("/signup", (req, res)=>{
	console.log(req.body);
	// if contents of the request body with the property "username" and "password" is not empty
	if(req.body.username !== '' && req.body.password !== ''){
	// This will store the user object sent via POSTMAN to the users array created above
		users.push(req.body)
		res.send(`User ${req.body.username} successfully registered`)
		//send response
	}else{
		res.send("Please input BOTH username and password")
	}
})	

// Update the password of a user that matches the information provided in the client / POSTMAN
app.put("/change-password", (req, res)=>{
	let message;
	// create a for loop that will loop through the element of the "users" array
	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){
		//changes the password of the user found by the loop into the password provided in the client/POSTMAN
		users[i].password = req.body.password
		// pwedeng baguhin ang password
		// if users[i].username = req.body.username, pwedeng baguhin ang username
	
		//Changes the message to be sent back by the response

		message = `User ${req.body.username}'s password has been updated`
		break;
		// breaks out of the loop once a user that matches the username provided in the client/POSTMAN is found
		// para hindi mag-infinite loop ang for loop
		}else{
			message = "user doesnot exist"
		}
	}
	res.send(message);
})
// same dapat yung username sa POST and username sa PUT or vice-versa sa password


// START OF ACTIVITY

// Create a GET route that will access the "/home" route that will print out a simple message.
app.get("/home", (req, res)=>{
	res.send("Welcome to my Homepage!")
})

// Create a GET route that will access the "/users" route that will retrieve all the users in the mock database
app.get("/users", (req, res)=>{
	res.send(users);
})

// Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete("/delete-user", (req, res)=>{
	let importantMessage;

	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){ 
			users.splice(1, 1)
			importantMessage = `User ${users[i].username} has been deleted`
		}
	}
	res.send(importantMessage);
})

app.listen(port, () => 
	console.log(`Server is running at port ${port}`)
	)






























